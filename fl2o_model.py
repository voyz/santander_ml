from keras import Model, Input
from keras.layers import BatchNormalization, Flatten, Dropout, Dense, Embedding, Concatenate, ReLU, Reshape, \
    Multiply, Lambda, Activation

from keras import backend as K

import utils
from nn_utils import get_unique_id

cont_emb=(50,10)
cat_emb_szs = [6, 12]
momentum = 0.1
# momentum = 0.99
dropout_rate=0.08
n_emb_feat = 2
cont_emb_szs = (201, n_emb_feat)
common_dropout =  Dropout(rate=dropout_rate)

def build_cat(inputs_cat):
    embedds = []
    for i in range(200):
        cropped = utils.Crop(1, i, i+1)(inputs_cat)
        embed = Embedding(cat_emb_szs[0], cat_emb_szs[1], name=get_unique_id('cat_embed'))(cropped)
        embedds.append(embed)
    x_cat_emb = Lambda(lambda x: K.stack(x, axis=1), name=get_unique_id('stack'))(embedds)

    cat_embed_model = Model(inputs=[inputs_cat], outputs=[x_cat_emb], name='cat_embed_model')
    x_cat_emb = cat_embed_model(inputs_cat)
    x_cat_emb = Reshape((200, cat_emb_szs[1]))(x_cat_emb)
    x_cat_emb = BatchNormalization(momentum=momentum)(x_cat_emb)
    x_cat_emb = common_dropout(x_cat_emb)


    return x_cat_emb

def build_cont(inputs_cont, interecept):
    x_feat_emb = Embedding(cont_emb_szs[0], cont_emb_szs[1])(interecept)
    x_feat_emb = Lambda(lambda x: K.stack(x, axis=1), name=get_unique_id('stack'))([x_feat_emb]*200)
    feat_emb_model = Model(inputs=[interecept], outputs=[x_feat_emb], name='feat_emb_model')
    x_feat_emb = feat_emb_model(interecept)
    x_feat_emb = BatchNormalization(momentum=momentum)(x_feat_emb)
    # x_feat_emb = Lambda(lambda x: K.reshape(x, (-1, 200, n_emb_feat)), name=get_unique_id('k.reshape'))(x_feat_emb)
    x_feat_emb = Lambda(lambda x: K.reshape(x, (-1, n_emb_feat)), name=get_unique_id('k.reshape'))(x_feat_emb)

    x_feat_w = Embedding(cont_emb_szs[0], cont_emb_szs[1])(interecept)
    x_feat_w = Lambda(lambda x: K.stack(x, axis=1), name=get_unique_id('stack'))([x_feat_w]*200)
    feat_w_model = Model(inputs=[interecept], outputs=[x_feat_w], name='feat_w_model')
    x_feat_w = feat_w_model(interecept)
    x_feat_w = BatchNormalization(momentum=momentum)(x_feat_w)

    x_cont_raw = utils.Crop(1, 0, 200)(inputs_cont)
    # x_cont_raw = Lambda(lambda x: K.batch_flatten(x), name=get_unique_id('k.batch_flatten'))(x_cont_raw)
    # x_cont_raw = Reshape((1,1))(x_cont_raw)
    x_cont_raw = Lambda(lambda x: K.reshape(x, (-1, 1)), name=get_unique_id('k.reshape'))(x_cont_raw)

    # x_cont_raw = Flatten()(x_cont_raw)
    # x_cont_raw = Reshape((200, 1))(x_cont_raw)
    # x_cont_raw = Concatenate(axis=2)([x_cont_raw, x_feat_emb])
    x_cont_raw = Concatenate(axis=1)([x_cont_raw, x_feat_emb])
    x_cont_raw = Dense(cont_emb[0], activation='relu')(x_cont_raw)
    x_cont_raw = Dense(cont_emb[1], activation=None)(x_cont_raw)
    x_cont_raw = Lambda(lambda x: K.reshape(x, (-1, 200, cont_emb[1])), name=get_unique_id('k.reshape'))(x_cont_raw)
    x_cont_raw = BatchNormalization(momentum=momentum)(x_cont_raw)
    x_cont_raw = common_dropout(x_cont_raw)

    x_cont_notu = utils.Crop(1, 200, 400)(inputs_cont)
    # x_cont_notu = Reshape((200, 1))(x_cont_notu)
    # x_cont_notu = Lambda(lambda x: K.batch_flatten(x), name=get_unique_id('k.batch_flatten'))(x_cont_notu)
    # x_cont_notu = Reshape((1,1))(x_cont_notu)
    x_cont_notu = Lambda(lambda x: K.reshape(x, (-1, 1)), name=get_unique_id('k.reshape'))(x_cont_notu)
    # x_cont_notu = Flatten()(x_cont_notu)
    # x_cont_notu = Concatenate(axis=2)([x_cont_notu, x_feat_emb])
    x_cont_notu = Concatenate(axis=1)([x_cont_notu, x_feat_emb])
    x_cont_notu = Dense(cont_emb[0], activation='relu')(x_cont_notu)
    # x_cont_notu = ReLU()(x_cont_notu)
    x_cont_notu = Dense(cont_emb[1], activation=None)(x_cont_notu)
    # x_cont_notu = Reshape((200, cont_emb[1]))(x_cont_notu)
    x_cont_notu = Lambda(lambda x: K.reshape(x, (-1, 200, cont_emb[1])), name=get_unique_id('k.reshape'))(x_cont_notu)
    x_cont_notu = BatchNormalization(momentum=momentum)(x_cont_notu)
    x_cont_notu = common_dropout(x_cont_notu)

    return x_feat_w, x_cont_raw, x_cont_notu


def fl2o_model(input_dim_cat, input_dim_cont):

    inputs_cat = Input(shape=[input_dim_cat], name='inputs_cat')
    inputs_cont = Input(shape=[input_dim_cont], name='inputs_cont')
    interecept = Input(shape=[1], name='intercept')
    # interecept = utils.Crop(1, 200, 201)(inputs_cat)
    # inputs_cat = utils.Crop(1, 0, 200)(inputs_cat)

    b_size = K.shape(inputs_cat)[0]


    x_cat = build_cat(inputs_cat)
    x_feat_w, x_cont_raw, x_cont_notu = build_cont(inputs_cont, interecept)

    # x_haso_emb = Reshape((12, 1))(Flatten()(x_haso_emb))
    # x_feat_w = Reshape((2, 1))(Flatten()(x_feat_w))
    # x_cont_raw = Reshape((10, 1))(Flatten()(x_cont_raw))
    # x_cont_notu = Reshape((10, 1))(Flatten()(x_cont_notu))
    x_cat_flat = Lambda(lambda x: K.reshape(x, (-1, 12)), name=get_unique_id('k.reshape'))(x_cat)
    x_feat_w_flat = Lambda(lambda x: K.reshape(x, (-1, 2)), name=get_unique_id('k.reshape'))(x_feat_w)
    x_cont_raw_flat = Lambda(lambda x: K.reshape(x, (-1, 10)), name=get_unique_id('k.reshape'))(x_cont_raw)
    x_cont_notu_flat = Lambda(lambda x: K.reshape(x, (-1, 10)), name=get_unique_id('k.reshape'))(x_cont_notu)


    x_w = Concatenate(axis=1)([x_cat_flat, x_feat_w_flat, x_cont_raw_flat, x_cont_notu_flat])
    # x_w = Concatenate(axis=2)([x_cat, x_cont_raw, x_cont_notu])
    x_w = BatchNormalization(momentum=momentum)(x_w)
    x_w = Dense(5, activation='relu')(x_w)
    x_w = Dense(1, activation=None)(x_w)
    x_w = Lambda(lambda x: K.reshape(x, (b_size, 200)), name=get_unique_id('k.reshape'))(x_w)
    x_w = Activation('softmax')(x_w)
    x_w = Lambda(lambda x : K.expand_dims(x, -1), name=get_unique_id('expand_dims'))(x_w)

    x_cat = Multiply()([x_w, x_cat])
    x_cont_raw = Multiply()([x_w, x_cont_raw])
    x_cont_notu = Multiply()([x_w, x_cont_notu])

    x_cat = Lambda(lambda x: K.sum((x), axis=1), name=get_unique_id('sum'))(x_cat)
    x_cont_raw = Lambda(lambda x: K.sum(x, axis=1), name=get_unique_id('sum'))(x_cont_raw)
    x_cont_notu = Lambda(lambda x: K.sum(x, axis=1), name=get_unique_id('sum'))(x_cont_notu)

    print(x_cat)
    print(x_cont_raw)
    print(x_cont_notu)
    x = Concatenate(axis=1)([x_cat, x_cont_raw, x_cont_notu])
    print(x)
    x = BatchNormalization(momentum=momentum)(x)


    # x = BatchNormalization()(x)
    x = Dense(32, activation='relu')(x)
    x = BatchNormalization(momentum=0.1)(x)
    x = Dropout(rate=0.2)(x)
    x = Dense(1)(x)

    # x = x_cat

    model = Model(
        inputs=[inputs_cat, inputs_cont, interecept],
        outputs=[x],
        name='model')

    return model