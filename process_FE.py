import pandas as pd

extra_features_filepath = './FE/extra_features[TYPE]_04.csv'


features = [f'var_{i}' for i in range(200)]
value_count_FE = [f'var_{i}_FE' for i in range(200)]
has_zero = [f'var_{i}_has_zero' for i in range(200)]
has_one = [f'var_{i}_has_one' for i in range(200)]
not_unique = [f'var_{i}_not_unique' for i in range(200)]
mul = [f'var_{i}_mul' for i in range(200)]
div = [f'var_{i}_div' for i in range(200)]
singular = ['ID_code','target', 'row_uniqueness']

wanted_cols = singular + features + value_count_FE + has_one + not_unique + mul + div

train = pd.read_csv(extra_features_filepath.replace('[TYPE]','_train'))
test = pd.read_csv(extra_features_filepath.replace('[TYPE]','_test'))
# train_small = train[:1024]
train_wanted = train[wanted_cols]
test_wanted = test[wanted_cols]
# print(train_wanted.columns.tolist())

# train_wanted.to_csv(extra_features_filepath.replace('[TYPE]','_train_sorted'), index=False)
# test_wanted.to_csv(extra_features_filepath.replace('[TYPE]','_test_sorted'), index=False)

train_small = train_wanted[:1024]
test_small = test_wanted[:1024]

train_medium = train_wanted[:10240]
test_medium = test_wanted[:10240]


train_medium.to_csv(extra_features_filepath.replace('[TYPE]','_train_medium'), index=False)
test_medium.to_csv(extra_features_filepath.replace('[TYPE]','_test_medium'), index=False)
