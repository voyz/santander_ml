from keras import Input, Model
from keras.callbacks import TensorBoard
from keras.layers import Dense, Flatten, GaussianNoise, Dropout, Reshape
from keras.utils import plot_model
from sklearn.metrics import r2_score


def make_dae_model(X_train, variant):
    latent_dim = 300
    input_dim = X_train.shape[1]

    # input
    inputs = Input(shape=[input_dim], name='inputs')
    x = inputs

    # test noise
    # x = GaussianNoise(0.7)(x)
    # x = GaussianNoise(0.1)(x)
    # x = GaussianNoise(0.15)(x)
    # x = Dropout(0.7)(x)

    def encoder_multidim(encoder_input):
        # encoder
        x = Reshape((input_dim, 1))(encoder_input)
        x = Dense(2, activation='relu')(x)
        x = Reshape((input_dim, 2, 1))(x)
        x = Dense(20, activation='relu')(x)
        x = Flatten()(x)

        encoder = Model(encoder_input, x, name='encoder')
        return encoder

    def encoder_single_big(encoder_input):
        x = Dense(1000, activation='relu')(encoder_input)
        encoder = Model(encoder_input, x, name='encoder')
        return encoder

    def encoder_single_medium(encoder_input):
        x = Dense(400, activation='relu')(encoder_input)
        encoder = Model(encoder_input, x, name='encoder')
        return encoder

    def encoder_deep_medium(encoder_input):
        x = Dense(400, activation='relu')(encoder_input)
        x = Dense(400, activation='relu')(x)
        x = Dense(400, activation='relu')(x)
        x = Dense(400, activation='relu')(x)
        encoder = Model(encoder_input, x, name='encoder')

        return encoder

    def encoder_bottleneck(encoder_input):
        x = Dense(1000, activation='relu')(encoder_input)
        x = Dense(500, activation='relu', name='dense_bottleneck')(x)
        x = Dense(1000, activation='relu')(x)
        encoder = Model(encoder_input, x, name='encoder')
        return encoder

    if variant.model_type == 'multidim':
        encoder = encoder_multidim(inputs)
    elif variant.model_type == 'single_big':
        encoder = encoder_single_big(inputs)
    elif variant.model_type == 'single_medium':
        encoder = encoder_single_medium(inputs)
    elif variant.model_type == 'deep_medium':
        encoder = encoder_deep_medium(inputs)
    elif variant.model_type == 'bottleneck':
        encoder = encoder_bottleneck(inputs)
    else:
        raise ValueError('Unrecognised model type: {0}'.format(variant.model_type))

    # output
    x = encoder(inputs)
    x = Dense(input_dim, activation='linear')(x)

    # loss

    def r_square(y_true, y_pred):
        from keras import backend as K
        SS_res =  K.sum(K.square(y_true - y_pred))
        SS_tot = K.sum(K.square(y_true - K.mean(y_true)))
        return ( 1 - SS_res/(SS_tot + K.epsilon()) )

    # model
    model = Model(inputs=[inputs], outputs=[x], name='dae_model')
    model.compile(loss='mse', optimizer='adam', metrics=['accuracy', r_square])
    model.summary()
    # plot_model(model, to_file='outputs/' + 'model-dae.png', show_shapes=True)
    return model