import os

import pandas as pd
from keras.callbacks import TensorBoard
from keras.utils import plot_model

from Attempt import Attempt
from Logger import Logger
from SwapNoiseGenerator import SwapNoiseGenerator
from dae import dae_models
from sklearn.metrics import mean_squared_error
from sklearn.metrics import accuracy_score
from sklearn.metrics import r2_score
import numpy as np

import time

class DaeAttempt (Attempt):
    def _body(self, variant):
        pass

        X_train, X_test, y_train, y_test, test_features = variant.data

        # its an autoencoder, so we can use the test features to train it too
        # X_train = np.concatenate((X_train, test_features))

        # X_train = X_train[0:200, :]

        sample_count = X_train.shape[0]
        input_dim = X_train.shape[1]


        print(X_train.shape)

        callbacks = []
        if variant.save:
            tensorboardCallback = TensorBoard(log_dir=(variant.logs))
            logger = Logger(patience=15,
                        out_path=variant.saves,
                        out_filename=variant.full_name())
            callbacks.append(tensorboardCallback)
            callbacks.append(logger)

        model = dae_models.make_dae_model(X_train, variant)

        if variant.save:
            plot_base_path = variant.outputs
            os.makedirs(plot_base_path, exist_ok=True)
            plot_base_path += '/'+variant.name
            plot_model(model, to_file='{0}.png'.format(plot_base_path), show_shapes=True)

        start_time = time.time()

        indexes = np.arange(sample_count)
        swapNoiseGenerator = SwapNoiseGenerator(indexes, X_train, batch_size=variant.batch_size, dim=input_dim,swap_ratio=0.15)

        model.fit_generator(swapNoiseGenerator, epochs=variant.epochs, validation_data=(X_test, X_test), callbacks=callbacks, verbose=2)

        # model.fit(X_train, X_train, epochs=variant.epochs, batch_size=variant.batch_size, validation_data=(X_test, X_test), callbacks=callbacks, verbose=2)
        print("--- %s seconds ---" % (round(time.time() - start_time, 1)))

        if variant.save:
            plot_base_path = variant.plots
            os.makedirs(plot_base_path, exist_ok=True)
            plot_base_path += '/'+variant.full_name()
            plot_model(model, to_file='{0}.png'.format(plot_base_path), show_shapes=True)

        # print(X_train)
        # print(X_train.shape)
        # train_pred = model.predict(X_train)
        # print(train_pred)
        # print(train_pred.shape)

        # def acc_fn(y_true, y_pred):
        #     return np.mean(np.equal(np.argmax(y_true, axis=-1), np.argmax(y_pred, axis=-1)))

        train_acc = r2_score(X_train, model.predict(X_train))
        print('train_acc: {0}'.format(round(train_acc, 4)))
        val_acc = r2_score(X_test, model.predict(X_test))
        print('val_acc: {0}'.format(round(val_acc, 4)))
        # test_acc = r2_score(test_features, model.predict(test_features))
        # print('test_acc: {0}'.format(round(test_acc, 4)))

        sample = X_test[0, :]
        pred = model.predict([[sample]])[0]
        print(sample[0:20])
        print(pred[0:20])
        print(np.abs(sample[0:20] - pred[0:20]))