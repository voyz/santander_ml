from keras import Input, Model
from keras.callbacks import TensorBoard
from keras.layers import Dense, Flatten, GaussianNoise, Dropout, Reshape
from keras.utils import plot_model

import utils

from Logger import Logger


def make_dae_model(X_train):
    latent_dim = 300
    input_dim = X_train.shape[1]

    # input
    inputs = Input(shape=[input_dim], name='inputs')
    x = inputs

    # swap noise
    # x = GaussianNoise(0.7)(x)
    # x = GaussianNoise(0.1)(x)
    x = GaussianNoise(0.15)(x)
    # x = Dropout(0.7)(x)


    # encoder
    x = Reshape((input_dim, 1))(x)
    x = Dense(2, activation='relu')(x)
    x = Reshape((input_dim, 2, 1))(x)
    x = Dense(20, activation='relu')(x)
    x = Flatten()(x)
    # x = Dense(400, activation='relu')(x)
    # x = Dense(1000, activation='relu')(x)

    # latent space
    # x = Dense(latent_dim, activation='relu')(x)

    # decoder
    # x = Dense(input_dim, activation='relu')(x)

    # output
    x = Dense(input_dim, activation='linear')(x)

    # loss

    # model
    model = Model(inputs=[inputs], outputs=[x], name='dae_model')
    model.compile(loss='mse', optimizer='adam', metrics=['accuracy'])
    model.summary()
    plot_model(model, to_file='outputs/' + 'model-dae.png', show_shapes=True)
    return model


def train_dae(model, X_train, X_test):
    timestamp = utils.get_timestamp()

    tensorboardCallback = TensorBoard(log_dir='logs/'+timestamp+'dae')
    logger = Logger(patience=15, out_path='outputs/models/', out_filename='model_%s-dae.h5' % timestamp)

    callbacks = []
    # callbacks.append(tensorboardCallback)
    callbacks.append(logger)

    model.fit(X_train, X_train, epochs=150, batch_size=128, validation_data=(X_test, X_test), callbacks=callbacks, verbose=2)

    pred = model.predict([[X_test[0, :]]])
    print(X_test[0, :][0:20])
    print(pred[0][0:20])