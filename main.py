import copy
import gc
import math

import tensorflow as tf
import pandas as pd
import os

from tqdm import tqdm
from keras.callbacks import TensorBoard
from keras.utils import plot_model
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from keras import Sequential, Input, Model
from keras import layers
from keras import backend as K
from keras.layers.core import Dense
from keras import regularizers
from keras.layers import Dropout, Add
from keras.constraints import max_norm
import numpy as np

import fl2o_model
import fl2o_model_simplified
from AucLogger import AucLogger
from Bridge import Bridge
from dae import dae
from inspect import stack

# CONFIG
import models
import utils
from Logger import Logger
from dae.DaeAttempt import DaeAttempt
from dae.DaeVariant import DaeVariant

ROOT_PATH = os.path.dirname(os.path.abspath(stack()[0][1]))

# batch_size = 64
# batch_size = 32768
# batch_size = 16384
# batch_size = 8192
# batch_size = 4096
# batch_size = 2048
batch_size = 1024
# batch_size = 512
# batch_size = 256
# batch_size = 128
# epochs = 250
# epochs = 125
epochs = 50
# epochs = 20
# epochs = 2

# END CONFIG

timestamp = utils.get_timestamp()

# Import data
# train = pd.read_csv('sources/train.csv')
# test = pd.read_csv('sources/test.csv')
# test['target'] = -1


recalculate_real_indices = False
real_indices_filepath = './FE/real_indices.npy'

recalculate_value_counts = False
value_counts_filepath = './FE/value_counts[TYPE]_02.csv'

recalculate_extra_features = False
# extra_features_filepath = './FE/extra_features[TYPE]_small_04.csv'
extra_features_filepath = './FE/extra_features[TYPE]_medium_04.csv'
# extra_features_filepath = './FE/extra_features[TYPE]_sorted_04.csv'

add_pseudo_labels = False
pseudo_label_filepath = './FE/pseudo_labels_01.csv'

features = [f'var_{i}' for i in range(200)]
has_zero = [f'var_{i}_has_zero' for i in range(200)]
has_one = [f'var_{i}_has_one' for i in range(200)]

def make_not_unique_feat(df, comb):
    for i in tqdm(range(200), desc='Producing not_unique_feat features'):
        var = features[i]
        mean = np.round(df[var].mean(), 4)
        comb_var = comb[var].value_counts().to_dict()

        # if i % 20 == 0:
        #     print('Producing not_unique_feat features: %d' % i)


        f = lambda x : x if comb_var[x] != 1 else mean


        df[var+'_not_unique_feat'] = df[var].apply(f).values


def make_has_one_feats_fl2o(train, test):

    for var in tqdm(features, desc='Producing has_one train features'):
        train[var + '_has_one'] = 0
        train[var + '_has_zero'] = 0

        pos = train.loc[train['target'] == 1, var].value_counts()

        pos_two_more = set(pos.index[pos >= 2]) # 3
        pos_one_more = set(pos.index[pos >= 1]) # 1

        neg = train.loc[train['target'] == 0, var].value_counts()

        neg_two_more = set(neg.index[neg >= 2]) # 3
        neg_one_more = set(neg.index[neg >= 1]) # 2

        train.loc[train['target'] == 1, var+'_has_one'] = train.loc[train['target'] == 1, var].isin(pos_two_more).astype(int)
        train.loc[train['target'] == 0, var+'_has_one'] = train.loc[train['target'] == 0, var].isin(pos_one_more).astype(int)

        train.loc[train['target'] == 1, var+'_has_zero'] = train.loc[train['target'] == 1, var].isin(neg_one_more).astype(int)
        train.loc[train['target'] == 0, var+'_has_zero'] = train.loc[train['target'] == 0, var].isin(neg_two_more).astype(int)

    train.loc[:, has_one] = 2 * train.loc[:, has_one].values + train.loc[:, has_zero].values


    for var in tqdm(features, desc='Producing has_one test features'):
        test[var + '_has_one'] = 0
        test[var + '_has_zero'] = 0
        pos = train.loc[train['target'] == 1, var].unique()
        neg = train.loc[train['target'] == 0, var].unique()
        test.loc[:, var + '_has_one'] = test[var].isin(pos).astype(int)
        test.loc[:, var + '_has_zero'] = test[var].isin(neg).astype(int)

    test.loc[:, has_one] = 2 * test.loc[:, has_one].values + test.loc[:, has_zero].values



    for var in tqdm(features, desc='Producing not_unique train features'):
        comb_vc = comb[var].value_counts()

        non_unique = comb_vc.index[comb_vc != 1] # 2 or more in comb

        m_train = train[var].isin(non_unique) # indices of values that are non-unique (true or false / 1 or 0)
        train[var + '_not_unique'] = m_train * train[var] + (~m_train) * train[var].mean()

        m_test = test[var].isin(non_unique)
        test[var + '_not_unique'] = m_test * test[var] + (~m_test) * train[var].mean()

        train.loc[~m_train, var + '_has_one'] = 4 # 5
        test.loc[~m_test, var + '_has_one'] = 4 # 5

# print(train['var_0_not_unique_feat'].to_string())
def make_has_one_feats(df, train):
    m = 200000
    targets = train['target'].tolist()
    for i in tqdm(range(200), desc='Producing has_one features', position=0):
        var = features[i]
        curr_var = var+'_has_one'
        value_store = {}

        pos = train[train['target'] == 1][var+'_FE_bespoke']
        neg = train[train['target'] == 0][var+'_FE_bespoke']
        real_test_FE = real_test[var+'_FE_bespoke']

        # f_train = lambda x: 1 if (pos.get(x, 0) > 1 and neg.get(x, 0) == 0)                       else \
        #                 2 if (pos.get(x, 0) == 0 and neg.get(x, 0) > 1)                           else \
        #                 3 if (pos.get(x, 0) > 2  and neg.get(x, 0) > 2)                           else \
        #                 4 if (pos.get(x, 0) + neg.get(x, 0) == 1 and real_test_FE.get(x, 0) >= 1) else \
        #                 5 if (pos.get(x, 0) + neg.get(x, 0) + real_test_FE.get(x, 0) == 1)        else -1

        def f_train(x):
            positive = pos.get(x, 0)
            negative = neg.get(x, 0)
            if targets[x] == 1:
                positive -= 1
            else:
                negative -= 1

            if (positive >= 1 and negative == 0):
                return 1
            elif positive == 0 and negative >= 1:
                return 2
            elif positive + negative >= 2:
                return 3
            elif positive + negative == 0 and real_test_FE.get(x, 0) >=1:
                return 4
            elif positive + negative + real_test_FE.get(x, 0) == 0:
                return 5
            else:
                print('\033[91m', x, var, positive, negative)
                return -1

        # df[curr_var] = np.nan
        # for j in range(m):
        #     if math.isnan(df.at[j, curr_var]):
        #         has_one_val = f_train(j)
        #         curr_value = df.loc[j, var]
        #         df.loc[df[var] == curr_value, curr_var] = has_one_val

        for j in range(m):
            curr_value = df.loc[j, var]
            if curr_value in value_store:
                df.at[j, curr_var] = value_store[curr_value]
            else:
                has_one_val = f_train(j)
                df.at[j, curr_var] = has_one_val
                value_store[curr_value] = has_one_val

        # for j in range(m):
        #     df.at[j, curr_var] = f_train(j)



def make_mul():
    for i in tqdm(range(200), desc='Combining features mul'):
        # if i % 50 == 0:
        #     print('Combining features mul: %d' % i)
        # print("train['var_{}'.format(i)]", train['var_{}'.format(i)][0:3])
        # print("train['var_{}_FE'.format(i)", train['var_{}_FE'.format(i)][0:3])
        # print("mul", (train['var_{}'.format(i)] * train['var_{}_FE'.format(i)])[0:3])
        train['var_{}_mul'.format(i)] = train['var_{}'.format(i)] * train['var_{}_FE'.format(i)]
        test['var_{}_mul'.format(i)] = test['var_{}'.format(i)] * test['var_{}_FE'.format(i)]

def make_div():
    for i in tqdm(range(200), desc='Combining features div'):
        # if i % 50 == 0:
        #     print('Combining features div: %d' % i)
        train['var_{}_div'.format(i)] = train['var_{}'.format(i)] / train['var_{}_FE'.format(i)]
        test['var_{}_div'.format(i)] = test['var_{}'.format(i)] / test['var_{}_FE'.format(i)]

def make_row_uniqueness(df):
    counts = df.filter(like='_FE', axis=1)
    sc = StandardScaler()
    df['row_uniqueness'] = np.sum(sc.fit_transform(counts[counts.columns].astype(float)), axis=1)
    # for i in tqdm(range(200000)):
    #     if i % 50000 == 0:
    #         print('Calculating row uniqueness: %d' % i)
    #     df.at[i,'_row_unique'] = counts[i].sum()

if recalculate_extra_features:
    if recalculate_real_indices:
        test = pd.read_csv('sources/test.csv')
        test_values = test.values
        unique_samples = []
        unique_count = np.zeros_like(test_values)
        for feature in tqdm(range(test_values.shape[1])):
            _, index_, count_ = np.unique(test_values[:, feature], return_counts=True, return_index=True)
            unique_count[index_[count_ == 1], feature] += 1

        # Samples which have unique values are real the others are fake
        real_samples_indexes = np.argwhere(np.sum(unique_count, axis=1) > 0)[:, 0]
        synthetic_samples_indexes = np.argwhere(np.sum(unique_count, axis=1) == 0)[:, 0]
        np.save(real_indices_filepath, real_samples_indexes)
    else:
        real_samples_indexes = np.load(real_indices_filepath)
        print('loaded real sample indices')


    # print(len(real_samples_indexes))
    # print(len(synthetic_samples_indexes))
    # df_test_real = test_features[real_samples_indexes].copy()

    # joint_features = np.concatenate((train_features.values, df_test_real))



    if recalculate_value_counts:
        # FREQUENCY ENCODE
        def encode_FE(df_source, col, test, postfix):
            cv = df_source[col].value_counts()
            nm = col+postfix
            df_source[nm] = df_source[col].map(cv)
            if test is not None:
                test[nm] = test[col].map(cv)
                test[nm].fillna(0,inplace=True)

            if cv.max()<=255:
                df_source[nm] = df_source[nm].astype('uint8')
                if test is not None:
                    test[nm] = test[nm].astype('uint8')
            else:
                df_source[nm] = df_source[nm].astype('uint16')
                if test is not None:
                    test[nm] = test[nm].astype('uint16')
            return

        train = pd.read_csv('sources/train.csv')
        test = pd.read_csv('sources/test.csv')
        test['target'] = -1
        print(train.shape)
        print(test.shape)
        comb = pd.concat([train,test.loc[real_samples_indexes]],axis=0,sort=True)
        for i in range(200): encode_FE(comb,'var_'+str(i),test, '_FE')
        train = comb[:len(train)]; del comb
        # for i in range(200): encode_FE(train,'var_'+str(i), None, '_FE_bespoke')
        # for i in range(200): encode_FE(test,'var_'+str(i), None, '_FE_bespoke')
        print('Added 200 new magic features!')
        print(train.shape)
        print(test.shape)
        train.to_csv(value_counts_filepath.replace('[TYPE]','_train'), index=False)
        test.to_csv(value_counts_filepath.replace('[TYPE]','_test'), index=False)
    else:
        train = pd.read_csv(value_counts_filepath.replace('[TYPE]','_train'))
        test = pd.read_csv(value_counts_filepath.replace('[TYPE]','_test'))
        print('loaded value counts')


    real_test = test.loc[real_samples_indexes]

    if add_pseudo_labels:
        pseudo_labels = pd.read_csv(pseudo_label_filepath)
        pseudo_labels_cols = ['ID_code', 'target']
        pseudo_labels_cols = pseudo_labels_cols + features
        # print(pseudo_labels_cols)
        pseudo_labels = pseudo_labels[pseudo_labels_cols]
        train = pd.concat([train, pseudo_labels], sort=True)
        print(train.shape)


    comb = pd.concat([train,real_test], sort=True)
    # j = 0

    # make_not_unique_feat(train, comb)
    # make_not_unique_feat(test, comb)
    # make_has_one_feats(train, train)
    # make_has_one_feats(test, train)
    make_has_one_feats_fl2o(train, test)
    make_mul()
    make_div()
    make_row_uniqueness(train)
    make_row_uniqueness(test)

    train.to_csv(extra_features_filepath.replace('[TYPE]','_train'), index=False)
    test.to_csv(extra_features_filepath.replace('[TYPE]','_test'), index=False)
else:
    train = pd.read_csv(extra_features_filepath.replace('[TYPE]','_train'))
    test = pd.read_csv(extra_features_filepath.replace('[TYPE]','_test'))
    print('loaded extra features')
#
# real_samples_indexes = np.load(real_indices_filepath)
# real_test = test.loc[real_samples_indexes]
# comb = pd.concat([train,real_test], sort=True)
#
# make_not_unique_feat(test, comb)
# test.to_csv(extra_features_filepath.replace('[TYPE]','_test3'), index=False)


# for i in range(200):
#     var = features[i]
#     train.drop([var+'_has_one'], axis=1)
#     # test.drop(var+'_has_one')


# train.drop(train.filter(regex='var_[0-9]+_has_one+').columns, axis=1, inplace=True)
# train.drop(train.filter(regex='var_[0-9]+_not_unique_feat+').columns, axis=1, inplace=True)
# train.drop(train.filter(regex='var_[0-9]+_mul+').columns, axis=1, inplace=True)
# train.drop(train.filter(regex='var_[0-9]+_div+').columns, axis=1, inplace=True)
# train.drop(train.filter(regex='var_[0-9]+_FE+').columns, axis=1, inplace=True)
#
# has_one_train = train.filter(like='_has_one', axis=1)
# has_one_train[has_one_train < 3.0] = np.nan
# train.update(has_one_train, overwrite=True)
# vc = pd.value_counts(has_one_train.values.flatten())
# print(vc)

# print(train['var_0_has_one'])
# print(train['var_1_has_one'])

has_one = [f'var_{i}_has_one' for i in range(200)]
orig = [f'var_{i}' for i in range(200)]
not_u = [f'var_{i}_not_unique' for i in range(200)]
cont_vars = orig + not_u
cat_vars = has_one

train = train[orig+has_one+not_u+['target', 'ID_code']]
test = test[orig+has_one+not_u+['target', 'ID_code']]

# feat = ['intercept']
# train['intercept'] = 1
# train['intercept'] = train['intercept'].astype('category')
# test['intercept'] = 1
# test['intercept'] = test['intercept'].astype('category')
# for col in train.columns.tolist():
#     print(col)

sc = StandardScaler()
train_samples = len(train.index)
ref = pd.concat([train[cont_vars + cat_vars + ['target']], test[cont_vars + cat_vars]], sort=True)
ref[cont_vars] = sc.fit_transform(ref[cont_vars].values)
train = ref.iloc[:train_samples][orig+has_one+not_u+['target']]
test = ref.iloc[train_samples:][orig+has_one+not_u+['target']]
# train = ref.iloc[:200000]
# test = ref.iloc[200000:]
# train = ref.iloc[:1000]
# test = ref.iloc[1000:]
# train = ref.iloc[:10000]
# test = ref.iloc[10000:]
train['target'] = train['target'].astype('int')
del ref; gc.collect()


print(train.columns.tolist())


train_features = train.drop(['target'], axis=1).values
test_features = test.drop(['target'], axis=1).values

train_targets = train['target']
# test_features = test_features.values
# print(train_features[0])



s = 0
# print(train_features[samples_start:samples_start+3,5], train_features[samples_start:samples_start+3,205])
# print(train_features[s:s+3, 5], train_features[s:s+3, 204:207], train_features[s:s+3, 404:407], train_features[s:s+3, 604:607])

# train_features = np.array([train_features[:, :200], train_features[:, 200:]])

# X_train, X_test, y_train, y_test = train_test_split(train_features, train_targets, test_size = 0.25, random_state = 50, shuffle=True, stratify=train_targets.values)
X_train, X_test, y_train, y_test = train_test_split(train_features, train_targets, test_size = 0.25, random_state = 50, shuffle=True)

print(X_train.shape)
print(y_train.shape)
# print(X_train[0])
# print(train_features[0])



## Feature Scaling
# from sklearn.preprocessing import StandardScaler
# sc = StandardScaler()
# X_train = X_train[:, :800]
# X_test = X_test[:, :800]
# X_train = sc.fit_transform(X_train)
# X_test = sc.transform(X_test)
# sc = StandardScaler()

# zeros = np.zeros((200000, 400))
# test_features = np.concatenate((test_features, zeros), axis=1)
# test_features = np.insert(test_features, 400, zeros, axis=1)
# temp = test_features[:, :400]
# temp = np.concatenate((temp, zeros), axis=1)
# temp = np.concatenate((temp, test_features[:, 400:]), axis=1)
# test_features = temp
# test_features = sc.transform(test_features)
# X_train[:, :, 0] = sc.fit_transform(X_train[:, :, 0])
# X_test[:, :, 0] = sc.transform(X_test[:, :, 0])
# test_features[:, :, 0] = sc.transform(test_features[:, :, 0])
input_dim = X_train.shape[1]
# print(X_train.shape)
# print(X_train[0].shape)
# print(X_train[0, ...].shape)

# X_train = np.swapaxes(np.reshape(X_train, (-1, 6, 200)), 1,2)
# X_test = np.swapaxes(np.reshape(X_test, (-1, 6, 200)), 1,2)
# test_features = np.swapaxes(np.reshape(test_features, (-1, 6, 200)), 1,2)

# X_train = np.reshape(X_train, (-1, 1200))
# X_test = np.reshape(X_test, (-1, 1200))
# test_features = np.reshape(test_features, (-1, 1200))
# print(X_train[0])

# print(X_train[samples_start:samples_start+3,5,0], X_train[samples_start:samples_start+3,5,1])
# print(X_train[s:s+3, 5, 0], X_train[s:s+3, 5, 1], X_train[s:s+3, 5, 2], X_train[s:s+3, 5, 3])
# print(X_train[s:s+3, 1, 0:4])
# print(X_train[123,5:8,0], X_train[123,5:8,1])
# print(X_train[123,5:8,0], X_train[123,5:8,1], X_train[123,5:8,2], X_train[123,5:8,3])


# dae_batch_size = 128
# # dae_epochs = 1
# # dae_epochs = 5
# dae_epochs = 50
# # dae_epochs = 150

# data = (X_train, X_test, y_train, y_test, test_features)
# dae_variant01 = DaeVariant(
#     name='dae',
#     title='snoise',
#     model_type='bottleneck',
#     epochs=dae_epochs,
#     root=os.path.join(ROOT_PATH),
#     timestamp=timestamp,
#     batch_size=dae_batch_size,
#     data = data,
#     save=True
# )
#
# DaeAttempt(dae_variant01).run()
#
# dae_variant02 = copy.copy(dae_variant01)
# dae_variant02.model_type = 'single_big'
# DaeAttempt(dae_variant02).run()
#
# dae_variant03 = copy.copy(dae_variant01)
# dae_variant03.model_type = 'single_medium'
# DaeAttempt(dae_variant03).run()
#
# dae_variant04 = copy.copy(dae_variant01)
# dae_variant04.model_type = 'deep_medium'
# DaeAttempt(dae_variant04).run()
#
# dae_variant05 = copy.copy(dae_variant01)
# dae_variant05.model_type = 'multidim'
# DaeAttempt(dae_variant05).run()


def sub_model_generator(model):
    inputs = Input(shape=[input_dim,], name='inputs2')

    # gaussian_noise_1 = model.get_layer('gaussian_noise_1')
    # reshape_1 = model.get_layer('reshape_1')
    # dense_1 = model.get_layer('dense_1')
    # reshape_2 = model.get_layer('reshape_2')
    # dense_2 = model.get_layer('dense_2')
    # flattened = model.get_layer('flatten_1')

    # x = flattened(dense_2(reshape_2(dense_1(reshape_1(gaussian_noise_1(inputs))))))

    encoder = model.get_layer('encoder')
    x = encoder(inputs)


    return Model(inputs, x, name='dae-test')

# bridge_source = "dae_0318-1424_e150_tests"
# bridge_source = "dae_0319-0136_e150_snoise_multidim"
bridge_source = "dae_0516-2144_e50_snoise_single_big"
# bridge_source = "dae_0319-0136_e150_snoise_single_big"

# source_path = os.path.join('outputs', bridge_source + '/saves/' + bridge_source)
# bridge = Bridge(source_path, sub_model_generator=sub_model_generator)

# dae_X_train = bridge.predict(X_train)
# dae_X_test = bridge.predict(X_test)
# print(dae_X_train)
# print(dae_X_train.shape)
# print(dae_X_test.shape)

# X_train = dae_X_train
# X_test = dae_X_test

input_dim = X_train.shape[1]



# Add RUC metric to monitor NN
def auc(y_true, y_pred):
    auc = tf.metrics.auc(y_true, y_pred)[1]
    K.get_session().run(tf.local_variables_initializer())
    return auc
    # return tf.py_function(roc_auc_score, (y_true, y_pred), tf.double)

# print(input_dim)

# Try early stopping
#from keras.callbacks import EarlyStopping
#callback = EarlyStopping(monitor='loss', min_delta=0, patience=10, verbose=0, mode='auto', baseline=None, restore_best_weights=True)

# inputs = Input(shape=[input_dim], name='inputs')
inputs = Input(shape=[input_dim, 1], name='inputs')
# inputs = Input(shape=[input_dim, 2], name='inputs')
# inputs = Input(shape=[input_dim, 4], name='inputs')
# inputs = Input(shape=[input_dim, 6], name='inputs')

# x = models.model_simple(inputs)
# x = models.model_default(inputs)
# x = models.model_split(inputs)
# x = models.model_resnext(inputs)
# x = models.model_flattened(inputs)
# x = models.model_flattened_bis(inputs)
# x = models.model_flattened_turbo(inputs)
x = models.model_flattened_turbo_test(inputs)
# x = models.model_convolutional(inputs)
# x = models.model_conv_parallel(inputs)
# x = models.model_3d(inputs)

# x = Add()([x, x1])

# model = Model(
#     inputs=[inputs],
#     outputs=[x],
#     name='model')

# model = fl2o_model.fl2o_model(200, 400)
model = fl2o_model_simplified.fl2o_model(200, 400)
# model = fl2o_model_simplified.fl2o_model(200, 600)

model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', auc])
# model.compile(loss='binary_crossentropy', optimizer='sgd', metrics=['accuracy', auc])
model.summary()

plot_model(model, to_file='outputs/' + 'model.png', show_shapes=True)





tensorboardCallback = TensorBoard(log_dir='logs/'+timestamp)
# logger = AucLogger(patience=15, out_path='./outputs/models/', out_filename='model_%s.h5' % timestamp, report_only_improvements=False)
logger = AucLogger(patience=15, report_only_improvements=False)

callbacks = []
callbacks.append(tensorboardCallback)
callbacks.append(logger)

# X_train = np.reshape(X_train, (-1, input_dim, 1))
# X_test = np.reshape(X_test, (-1, input_dim, 1))

X_cat_train = X_train[:, 200:400]
X_cont_train = np.concatenate([X_train[:, :200], X_train[:, 400:]], axis=1)
X_cat_test = X_test[:, 200:400]
X_cont_test = np.concatenate([X_test[:, :200], X_test[:, 400:]], axis=1)
intercept_train = np.ones((X_cat_train.shape[0], 1))
intercept_test = np.ones((X_cont_test.shape[0], 1))


print(X_cat_train.shape)
print(X_cont_train.shape)
# print(X_cont_train[1234])
# print(X_cat_train[531])
# checkpointLoad = 'outputs/models/model_0318-1650.h5--weights.h5'
# model.load_weights(checkpointLoad)

# model.fit(X_train, y_train, batch_size = batch_size, epochs = epochs, validation_data = (X_test, y_test), callbacks = callbacks, verbose=2)
model.fit(x=[X_cat_train, X_cont_train, intercept_train], y=y_train, batch_size = batch_size, epochs = epochs, validation_data = ([X_cat_test, X_cont_test, intercept_test], y_test), callbacks = callbacks, verbose=2)

# y_pred = model.predict(X_test)
# # y_pred = model.predict_proba(X_test)
# a = roc_auc_score(y_test, y_pred)
# print('ROC AUC: %f' % a)

# # Make predicitions
# pred = model.predict(test_features)
# pred_ = pred[:,0]
#
# print(pred_)
#
# # To CSV
# id_code_test = test['ID_code']
# my_submission = pd.DataFrame({"ID_code" : id_code_test, "target" : pred_})
#
# # print(my_submission)
#
# my_submission.to_csv('submission.csv', index = False, header = True)