import os
import sys

import pandas as pd
from keras.models import model_from_json
import numpy as np

from Bridge import Bridge

features = [f'var_{i}' for i in range(200)]
extra_features_filepath = './FE/extra_features[TYPE]_03.csv'
real_indices_filepath = './FE/real_indices.npy'

model_raw = 'model_0528-1603.h5'
model_dir = os.path.abspath('./outputs/models/')
model_path = os.path.join(model_dir, model_raw)

threshold_pos = 0.9
threshold_neg = 0.1

pseudo_label_output = './FE/pseudo_labels_01.csv'

def run():
    test = pd.read_csv(extra_features_filepath.replace('[TYPE]','_test'))
    real_samples_indexes = np.load(real_indices_filepath)
    test = test.loc[real_samples_indexes]

    indices = test.index.values
    test_features = test.drop(['ID_code', 'target'], axis=1).values

    bridge = Bridge(model_path)

    input_dim = test_features.shape[1]
    test_features = np.reshape(test_features, (-1, input_dim, 1))

    y_pred = bridge.predict(test_features)
    # print(y_pred.shape)

    # unique, counts = np.unique(y_pred, return_counts=True)
    # np.set_printoptions(precision=4, threshold=sys.maxsize, suppress=True)
    # print(np.asarray((unique, counts)).T)
    # print(len(counts), len(unique))

    argsort = y_pred[:, 0].argsort()
    # test_features_sorted = test_features[argsort]
    indices_sorted = indices[argsort]
    # y_pred_sorted = y_pred[argsort]

    # neg = test_features_sorted[:extract_neg]
    # pos = test_features_sorted[-extract_pos:]

    cutoff_neg = (y_pred <= threshold_neg).sum()
    cutoff_pos = (y_pred >= threshold_pos).sum()
    print(cutoff_neg)
    print(cutoff_pos)

    indices_neg = indices_sorted[:cutoff_pos]#[:, 0]
    indices_pos = indices_sorted[-cutoff_pos:]#[:, 0]

    # print(indices_neg.shape)
    # print(indices_neg)
    # print(argsort.shape)
    # print(argsort)

    test_neg = test.loc[indices_neg]
    test_pos = test.loc[indices_pos]

    test_neg['target'] = 0
    test_pos['target'] = 1




    pseudo_labels = pd.concat([test_neg, test_pos])
    pseudo_labels_cols = ['ID_code', 'target']
    pseudo_labels_cols = pseudo_labels_cols + features
    pseudo_labels = pseudo_labels[pseudo_labels_cols]

    pseudo_labels = pseudo_labels.sample(frac=1).reset_index(drop=True)
    cols = pseudo_labels.columns.tolist()
    cols.remove('target')
    cols.remove('ID_code')
    cols.insert(0, 'target')
    cols.insert(0, 'ID_code')
    # print(cols)
    pseudo_labels = pseudo_labels[cols]


    # print(test_neg.shape)
    # print(test_pos.shape)

    print(test_neg.iloc[0:4])
    print(test_pos.iloc[0:4])

    pseudo_labels.to_csv(pseudo_label_output, index=False)


def test():
    pseudo_labels = pd.read_csv(pseudo_label_output)

    pos = pseudo_labels[pseudo_labels['target'] == 1]
    neg = pseudo_labels[pseudo_labels['target'] == 0]

    pos_vals = pos.drop(['ID_code', 'target'], axis=1).values
    neg_vals = neg.drop(['ID_code', 'target'], axis=1).values

    bridge = Bridge(model_path)

    input_dim = pos_vals.shape[1]
    pos_vals = np.reshape(pos_vals, (-1, input_dim, 1))
    neg_vals = np.reshape(neg_vals, (-1, input_dim, 1))

    pos_pred = bridge.predict(pos_vals)
    neg_pred = bridge.predict(neg_vals)

    print(pos_pred)
    print(pos_pred.shape)
    print(neg_pred)
    print(neg_pred.shape)

    print((pos_pred > 0.9).sum())


    np.set_printoptions(precision=4, threshold=sys.maxsize, suppress=True)
    unique, counts = np.unique(pos_pred, return_counts=True)
    print(np.asarray((unique, counts)).T)
    print(len(counts), len(unique))

    unique, counts = np.unique(neg_pred, return_counts=True)
    print(np.asarray((unique, counts)).T)
    print(len(counts), len(unique))

# run()
# test()



