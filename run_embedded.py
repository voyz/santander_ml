import gc

import pandas as pd
import numpy as np
import tensorflow as tf
from keras import backend as K
from keras.callbacks import TensorBoard
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

import fl2o_model_simplified
import utils
from AucLogger import AucLogger
import embedded_model
from keras.utils import plot_model


# Add RUC metric to monitor NN
def auc(y_true, y_pred):
    auc = tf.metrics.auc(y_true, y_pred)[1]
    K.get_session().run(tf.local_variables_initializer())
    return auc
    # return tf.py_function(roc_auc_score, (y_true, y_pred), tf.double)


timestamp = utils.get_timestamp()

batch_size = 1024
# batch_size = 64
epochs = 50


# extra_features_filepath = './FE/extra_features[TYPE]_small_04.csv'
extra_features_filepath = './FE/extra_features[TYPE]_medium_04.csv'
# extra_features_filepath = './FE/extra_features[TYPE]_sorted_04.csv'

train = pd.read_csv(extra_features_filepath.replace('[TYPE]','_train'))
test = pd.read_csv(extra_features_filepath.replace('[TYPE]','_test'))

has_one = [f'var_{i}_has_one' for i in range(200)]
val_counts = [f'var_{i}_FE' for i in range(200)]
orig = [f'var_{i}' for i in range(200)]
not_u = [f'var_{i}_not_unique' for i in range(200)]
mul = [f'var_{i}_mul' for i in range(200)]
div = [f'var_{i}_div' for i in range(200)]

all_features = has_one + val_counts + orig + not_u + mul + div
cont_vars = val_counts + orig + not_u + mul + div

train = train[all_features+['target', 'ID_code']]
test = test[all_features+['target', 'ID_code']]

sc = StandardScaler()
train_samples = len(train.index)
ref = pd.concat([train[all_features + ['target']], test[all_features]], sort=True)
ref[cont_vars] = sc.fit_transform(ref[cont_vars].values)
train = ref.iloc[:train_samples][all_features+['target']]
test = ref.iloc[train_samples:][all_features+['target']]
train['target'] = train['target'].astype('int')
del ref; gc.collect()
train_targets = train['target']


X_train, X_test, y_train, y_test = train_test_split(train, train_targets, test_size = 0.25, random_state = 50, shuffle=True)

has_one_train = X_train[has_one].values
val_counts_train = X_train[val_counts].values
div_train = X_train[div].values
mul_train = X_train[mul].values
orig_train = X_train[orig].values
not_u_train = X_train[not_u].values

has_one_test = X_test[has_one].values
val_counts_test = X_test[val_counts].values
div_test = X_test[div].values
mul_test = X_test[mul].values
orig_test = X_test[orig].values
not_u_test = X_test[not_u].values

input_dim_has_one = has_one_train.shape[1]
input_dim_val_counts = val_counts_train.shape[1]
input_dim_div = div_train.shape[1]
input_dim_mul = mul_train.shape[1]
input_dim_orig = orig_train.shape[1]
input_dim_not_u = not_u_train.shape[1]

_, vocab_size_has_one = np.unique(has_one_train, return_counts=True)
_, vocab_size_val_counts = np.unique(val_counts_train, return_counts=True)
vocab_size_has_one = len(vocab_size_has_one) + 1
vocab_size_val_counts = len(vocab_size_val_counts) + 1

print(vocab_size_has_one)
print(vocab_size_val_counts)


model = embedded_model.build_embedded_model(input_dim_has_one, vocab_size_has_one, input_dim_val_counts, vocab_size_val_counts, input_dim_div, input_dim_mul, input_dim_orig, input_dim_not_u)

# model = fl2o_model_simplified.fl2o_model(200, 400)


model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy', auc])
print('compiled')
model.summary()
print('summaried')

plot_model(model, to_file='outputs/' + 'model-embedded.png', show_shapes=True)

tensorboardCallback = TensorBoard(log_dir='logs/'+timestamp)
logger = AucLogger(patience=15, report_only_improvements=False)

callbacks = []
callbacks.append(tensorboardCallback)
callbacks.append(logger)



intercept_train = np.ones((has_one_train.shape[0], 1))
intercept_test = np.ones((has_one_test.shape[0], 1))

print(has_one_train.shape)
print(val_counts_train.shape)
print(div_train.shape)
print(mul_train.shape)
print(orig_train.shape)
print(not_u_train.shape)


x_train = [has_one_train,
        val_counts_train,
        div_train,
        mul_train,
        orig_train,
        not_u_train,
        intercept_train]

x_test = [has_one_test,
        val_counts_test,
        div_test,
        mul_test,
        orig_test,
        not_u_test,
        intercept_test]

# X_cat_train = has_one_train
# X_cat_test = has_one_test
# X_cont_train = np.concatenate([orig_train, not_u_train], axis=1)
# X_cont_test = np.concatenate([orig_test, not_u_test], axis=1)

# all_inputs_train = np.concatenate([has_one_train, val_counts_train, div_train, mul_train, orig_train, not_u_train], axis=1)
# all_inputs_test = np.concatenate([has_one_test, val_counts_test, div_test, mul_test, orig_test, not_u_test], axis=1)
# x_train = [all_inputs_train, intercept_train]
# x_test = [all_inputs_test, intercept_test]

model.fit(x=x_train, y=y_train, batch_size = batch_size, epochs = epochs, validation_data = (x_test, y_test), callbacks = callbacks, verbose=2)

# model.fit(x=[X_cat_train, X_cont_train, intercept_train], y=y_train, batch_size = batch_size, epochs = epochs, validation_data = ([X_cat_test, X_cont_test, intercept_test], y_test), callbacks = callbacks, verbose=2)
