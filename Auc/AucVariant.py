from Variant import Variant



class AucVariant (Variant):


    def __init__(self,
                 data,
                 model_type=None,
                 *args, **kwargs):
        self.data = data
        self.model_type = model_type

        super().__init__(*args, **kwargs)


    def full_name(self):
        if self.model_type != None:
            return "{0}_{1}".format(
                super().full_name(),
                self.model_type
            )
        else:
            return "{0}".format(
                super().full_name()
            )