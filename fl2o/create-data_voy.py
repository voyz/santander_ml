# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in

import pandas as pd
# import feather
import numpy as np

# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory

import os
# print(os.listdir("../input"))


train = pd.read_csv('../sources/train.csv', index_col=0)
# when using dataframe you can specify some columns that must not be used
not_used = []
cat_feat = []
target = 'target'

# likeli_feat = feather.read_dataframe('./data/likeli_10folds_1000.fth').set_index('ID_code')

# data = pd.merge(data, likeli_feat.iloc[:200000], left_index=True, right_index=True)
# features = [i for i in data.columns if i != target]


# print(data.shape, len(features))
# train.head()


test = pd.read_csv('../sources/test.csv')
# etd = pd.merge(etd, likeli_feat.iloc[200000:], left_index=True, right_index=True)
# test.head()


from tqdm import tqdm

features = [f'var_{i}' for i in range(200)]
has_one = [f'var_{i}_has_one' for i in range(200)]
has_zero = [f'var_{i}_has_zero' for i in range(200)]
not_u = [f'var_{i}_not_unique' for i in range(200)]

# for var in tqdm(features):
#     unique_v = test[var].value_counts()
#     unique_v = unique_v.index[unique_v == 1]
#     test[var + '_u'] = test[var].isin(unique_v)


# test['has_unique'] = test[[f + '_u' for f in features]].any(axis=1)
# print(test['has_unique'].sum())

real_indices_filepath = '../real_indices.npy'
real_samples_indices = np.load(real_indices_filepath)
print('loaded real sample indices')

# real_samples = test.loc[test['has_unique'], features]
real_samples = test.loc[real_samples_indices]
test = test.drop(['ID_code'], axis=1)

comb = pd.concat([train, real_samples], axis=0)
print(comb.shape)


# train = train[:1000]
# train.drop(train[1000:], inplace=True, axis=0)
# print(train.shape)

for var in tqdm(features):
    train[var + '_has_one'] = 0
    train[var + '_has_zero'] = 0

    pos = train.loc[train['target'] == 1, var].value_counts()

    pos_two_more = set(pos.index[pos >= 2]) # 3
    pos_one_more = set(pos.index[pos >= 1]) # 1

    neg = train.loc[train['target'] == 0, var].value_counts()

    neg_two_more = set(neg.index[neg >= 2]) # 3
    neg_one_more = set(neg.index[neg >= 1]) # 2

    train.loc[train['target'] == 1, var+'_has_one'] = train.loc[train['target'] == 1, var].isin(pos_two_more).astype(int)
    train.loc[train['target'] == 0, var+'_has_one'] = train.loc[train['target'] == 0, var].isin(pos_one_more).astype(int)

    train.loc[train['target'] == 1, var+'_has_zero'] = train.loc[train['target'] == 1, var].isin(neg_one_more).astype(int)
    train.loc[train['target'] == 0, var+'_has_zero'] = train.loc[train['target'] == 0, var].isin(neg_two_more).astype(int)

train.loc[:, has_one] = 2 * train.loc[:, has_one].values + train.loc[:, has_zero].values


for var in tqdm(features):
    test[var + '_has_one'] = 0
    test[var + '_has_zero'] = 0
    pos = train.loc[train[target] == 1, var].unique()
    neg = train.loc[train[target] == 0, var].unique()
    test.loc[:, var + '_has_one'] = test[var].isin(pos).astype(int)
    test.loc[:, var + '_has_zero'] = test[var].isin(neg).astype(int)

test.loc[:, has_one] = 2 * test.loc[:, has_one].values + test.loc[:, has_zero].values



for var in tqdm(features):
    comb_vc = comb[var].value_counts()

    non_unique = comb_vc.index[comb_vc != 1] # 2 or more in comb

    m_train = train[var].isin(non_unique) # indices of values that are non-unique (true or false / 1 or 0)
    train[var + '_not_unique'] = m_train * train[var] + (~m_train) * train[var].mean()

    m_test = test[var].isin(non_unique)
    test[var + '_not_unique'] = m_test * test[var] + (~m_test) * train[var].mean()

    train.loc[~m_train, var + '_has_one'] = 4 # 5
    test.loc[~m_test, var + '_has_one'] = 4 # 5


# train = pd.read_csv('./fl2o_data.csv')

# def test_has_one(val, max_val, comp):
#     for var in ['var_0']:
#         vc = train.loc[:, var].value_counts()
#         entries = train.loc[train[var + '_has_one'] == val, var]
#
#         for entry in entries:
#             if not comp(vc[entry], max_val):
#                 print(val, entry, vc[entry])

# smaller_eq = lambda x,y: x<=y
# equals = lambda x,y: x == y
# larger_eq = lambda x,y: x>=y
#
# test_has_one(0, 1, equals)
# test_has_one(1, 2, larger_eq)
# test_has_one(2, 2, larger_eq)
# test_has_one(3, 3, larger_eq)
# test_has_one(4, 1, equals)

# train['var_0_has_one'].value_counts()

has_one_train = train.filter(like='_has_one', axis=1)
vc = pd.value_counts(has_one_train.values.flatten())
print(vc)

# train.to_csv('./fl2o_data.csv', index=False)
# feather.write_dataframe(train.reset_index(), './921_data.fth')
# feather.write_dataframe(etd.reset_index(), './921_etd.fth')
# np.save('./real_samples.index', real_samples.index.values)