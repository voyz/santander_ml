import datetime

from keras.layers import Lambda

from nn_utils import get_unique_id


def get_timestamp():
    return datetime.datetime.now().strftime('%m%d-%H%M')

def Crop(dim, start, end, **kwargs):
    # Crops (or slices) a Tensor on a given dimension from start to end
    # example : to crop tensor x[:, :, 5:10]

    def func(x):
        dimension = dim
        if dimension == -1:
            dimension = len(x.shape) - 1
        if dimension == 0:
            return x[start:end]
        if dimension == 1:
            return x[:, start:end]
        if dimension == 2:
            return x[:, :, start:end]
        if dimension == 3:
            return x[:, :, :, start:end]
        if dimension == 4:
            return x[:, :, :, :, start:end]

    return Lambda(func, name=get_unique_id('crop'), **kwargs,)