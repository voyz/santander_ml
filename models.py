from keras import regularizers, Model, Input
from keras.constraints import max_norm
from keras.layers import Dense, Dropout, Add, Concatenate, LeakyReLU, BatchNormalization, Flatten, Reshape, Conv2D, \
    MaxPool2D, ReLU, Conv3D, MaxPool3D, Permute, Lambda
from keras import backend as K
from keras.utils import plot_model

import utils

unique_id_names={}
first_write = True

def get_unique_id(name='default'):
    global unique_id_names
    if name not in unique_id_names:
        unique_id_names[name] = 0
    else:
        unique_id_names[name] += 1
    # a = unique_id
    # unique_id += 1
    return name + '_%02d' % unique_id_names[name]

def default_dense(input, nodes, activation='relu'):

    # x = Dense(units = nodes, activation=activation, kernel_regularizer=regularizers.l2(0.001), kernel_constraint=max_norm(5))(input)
    # x = LeakyReLU()(x)
    x = Dense(units = nodes, activation=activation)(input)

    return x

def model_default(inputs):
    x = inputs
    x = Flatten()(x)
    x = Dense(200, activation='relu', kernel_initializer = "normal", kernel_regularizer=regularizers.l2(0.005), kernel_constraint = max_norm(5.))(x)
    x = Dropout(rate=0.2)(x)
    x = default_dense(x, 200)
    x = Dropout(rate=0.1)(x)
    x = default_dense(x, 100)
    x = Dropout(rate=0.1)(x)
    x = default_dense(x, 50, activation='tanh')
    x = Dropout(rate=0.1)(x)

    x = Dense(1, activation='sigmoid')(x)

    return x

def model_simple(inputs):
    x = inputs
    x = Flatten()(x)
    x = Dense(100, activation='relu')(x)

    x = Dense(1, activation='sigmoid')(x)
    return x


def residual_block(input, nodes=100, dropout_rate=0.1):
    input_nodes = K.int_shape(input)[1]
    residual = input
    r = default_dense(input, nodes)
    r = Dropout(rate=dropout_rate)(r)
    r = default_dense(r, nodes)
    r = Dropout(rate=dropout_rate)(r)
    r = default_dense(r, input_nodes)
    r = Dropout(rate=dropout_rate)(r)
    r = Add()([r, residual])
    return r

def make_branch(input, start, end, residual_nodes=50, dropout_rate=0):

    b = input
    b = utils.Crop(1, start, end)(b)

    # b = residual_block(b, residual_nodes, dropout_rate)
    # b = residual_block(b, residual_nodes, dropout_rate)
    # b = residual_block(b, residual_nodes, dropout_rate)
    # b = residual_block(b, residual_nodes, dropout_rate)

    b = default_dense(b, 50)
    # b = default_dense(b, 6)
    # b = Dropout(rate=dropout_rate)(b)

    return b

def make_stream(input, cardinality, overlap=0):
    input_nodes = K.int_shape(input)[1]
    stream_input= Input(shape=(input_nodes,), name='stream_input')
    x = stream_input
    branch_size = int(input_nodes / cardinality)

    branches = []
    for i in range(0, cardinality):
        start = i * branch_size
        end = (i+1) * branch_size

        if start > overlap-1:
            start -= overlap
        if end < input_nodes - (overlap+1):
            end += overlap

        # print(start, end)
        branches.append(make_branch(x, start, end))


    x = Concatenate()(branches)
    stream_model = Model(name=get_unique_id('stream_model'), inputs=[stream_input], outputs=[x])
    x = stream_model(input)

    global first_write
    if first_write:
        first_write = False
        stream_model.summary()
        plot_model(stream_model, to_file='outputs/' + 'stream_model.png', show_shapes=True)

    return x

def model_split(inputs):
    residual_nodes = 50
    dropout_rate = 0

    x = inputs
    x = BatchNormalization(axis=-2)(x)

    # x = default_dense(x, 16, activation='relu')
    # x = Dense(128, activation='relu')(x)
    x = Flatten()(x)
    # x = default_dense(x, 200)

    # x = Dropout(rate=0.3)(x)

    # flattened_inputs = Flatten()(inputs)

    # x = Concatenate()([x, flattened_inputs])

    # x = Dropout(rate=0.3)(x)

    # x = default_dense(x, 200)
    # x = Dropout(rate=dropout_rate)(x)
    # x = default_dense(x, 200)
    # x = Dropout(rate=dropout_rate)(x)

    resnext_residual = x
    residual_nodes = K.int_shape(x)[1]

    streams = []
    # streams.append(make_stream(x, 2))
    # streams.append(make_stream(x, 10))
    # streams.append(make_stream(x, 20))
    # streams.append(make_stream(x, 40))
    # streams.append(make_stream(x, 40))
    streams.append(make_stream(x, 40))
    streams.append(make_stream(x, 40))
    streams.append(make_stream(x, 40))
    # streams.append(make_stream(x, 60))
    # streams.append(make_stream(x, 200))
    # streams.append(make_stream(x, 200))
    # streams.append(make_stream(x, 200))
    # streams.append(make_stream(x, 200))

    for i in range(len(streams)):
        stream = streams[i]

        # stream = Dropout(rate=0.5)(stream)
        stream = default_dense(stream, 1000)
        # stream = default_dense(stream, residual_nodes)

        streams[i] = stream

    if len(streams) > 1:
        x = Add()(streams)

        # x = Concatenate()(streams)
        # x = Dropout(rate=0.5)(x)
        # x = Add()([x, resnext_residual])
    elif len(streams) == 1:
        x = streams[0]
    else:
        pass


    # x = inputs
    # x = default_dense(x, 500)
    residual = x

    residual_nodes = K.int_shape(x)[1]

    # x = default_dense(x, residual_nodes)
    # x = Dropout(rate=dropout_rate)(x)

    # x = Add()([x, residual])

    x = default_dense(x, 1000)
    x = Dropout(rate=dropout_rate)(x)

    # x = default_dense(x, 50)
    # x = Dropout(rate=dropout_rate)(x)


    # x = Reshape((-1, 50, 1))(x)
    # x = default_dense(x, 16)
    # x = Flatten()(x)

    x = Dense(1, activation='sigmoid')(x)
    return x


def resnext_dense(nodes):
    return Dense(units = nodes, activation=None, kernel_regularizer=regularizers.l2(0.005), kernel_constraint=max_norm(5))
    # return Dense(units = nodes, activation=None, use_bias=False)

def common_layers(x, activate=True):
    # x = BatchNormalization()(x)
    if activate:
        x = LeakyReLU()(x)
    x = Dropout(rate=0.1)(x)
    return x

def residual_group(input, cardinality=10, nodes=50):
    input_nodes = K.int_shape(input)[1]
    branch_input = Input(shape=(input_nodes,), name='branch_input')


    branch_size = int(input_nodes / cardinality)
    branches = []

    for i in range(0, cardinality):
        start = i * branch_size
        end = (i+1) * branch_size

        b = utils.Crop(1, start, end)(branch_input)


        b = resnext_dense(nodes)(b)
        b = common_layers(b)
        b = resnext_dense(nodes)(b)
        b = common_layers(b)

        branches.append(b)

    x = Concatenate()(branches)

    branch_model = Model(name=get_unique_id('branch_model'), inputs=[branch_input], outputs=[x])

    global first_write
    if first_write:
        first_write = False
        branch_model.summary()
        plot_model(branch_model, to_file='outputs/' + 'branch_model.png', show_shapes=True)

    x = branch_model([input])
    return x


def resnext_block(input, nodes=50):
    input_nodes = K.int_shape(input)[1]

    residual = input
    x = input

    # x = resnext_dense(nodes)(x)
    # x = common_layers(x)

    x = residual_group(x, cardinality=32, nodes=nodes)

    # x = resnext_dense(nodes)(x)
    x = resnext_dense(input_nodes)(x)
    x = common_layers(x, activate=False)

    # if nodes != input_nodes:
    #     residual = resnext_dense(nodes)(residual)
    #     residual = common_layers(residual, activate=False)

    x = Add()([x, residual])

    x = LeakyReLU()(x)

    return x

def model_resnext(inputs):
    x = inputs

    x = resnext_dense(200)(x)
    x = common_layers(x)

    for i in range(3):
        x = resnext_block(x, nodes=50)

    for i in range(4):
        x = resnext_block(x, nodes=100)
    #
    for i in range(4):
        x = resnext_block(x, nodes=200)
    #
    for i in range(3):
        x = resnext_block(x, nodes=400)

    # residual_input_nodes = K.int_shape(x)[1]
    # residual = x
    # x = resnext_dense(residual_input_nodes)(x)
    # x = common_layers(x, activate=False)
    # x = Add()([x, residual])
    # x = LeakyReLU()(x)
    x = resnext_dense(50)(x)
    x = common_layers(x)

    x = Dense(1, activation='sigmoid')(x)

    return x


def model_flattened(input):
    x = input
    x = BatchNormalization(axis=-2)(x)
    x = Dense(16, activation='relu')(input)
    x = Flatten()(x)
    x = Dense(1, activation='sigmoid')(x)
    return x

def model_flattened_bis(input):
    x = input
    x = BatchNormalization(axis=-2)(x)
    x = Dense(64, activation='relu')(x)
    x = Dense(32, activation='relu')(x)
    x = Flatten()(x)
    x = Dense(1, activation='sigmoid')(x)
    return x

def model_flattened_turbo(input):
    x = input
    x = BatchNormalization(axis=-2)(x)
    # x = Dense(128, activation='relu')(x)
    x = default_dense(x, 128, activation='relu')
    x = Flatten()(x)
    x = Dropout(rate=0.5)(x)
    x = Dense(1, activation='sigmoid')(x)
    return x

def model_flattened_turbo_test(input):
    x = input

    x = Flatten()(x)
    x = Dense(32, activation='relu')(x)
    x = Lambda(lambda x: K.expand_dims(x, -1))(x)
    x = BatchNormalization(axis=1)(x)
    x = Flatten()(x)
    # x = Dropout(rate=0.2)(x)
    x = Dense(1)(x)
    return x

def default_conv2d(input, filters, kernel_size=(3,3)):
    x = input
    x = Conv2D(filters, kernel_size, data_format='channels_first', padding='same')(x)

    # x = Conv2D(filters, kernel_size, data_format='channels_first', padding='same', kernel_regularizer=regularizers.l2(0.005), kernel_constraint=max_norm(5), activation=None)(x)
    # x = Conv2D(filters, kernel_size, data_format='channels_first', padding='same', activation=None)(x)
    # x = BatchNormalization()(x)
    # x = ReLU()(x)
    # x = LeakyReLU()(x)

    return x

def model_convolutional(input):
    input_nodes = K.int_shape(input)[1]
    res = 6
    x = Dense(res*res, activation='relu')(input)
    x = Reshape((input_nodes, res, res))(x)
    print(x)

    x = default_conv2d(x, 16, (3,3))
    # x = MaxPool2D(data_format='channels_first')(x)
    # x = Dropout(rate=0.2)(x)
    print(x)

    # x = default_conv2d(x, 16, (3,3))
    # x = MaxPool2D(data_format='channels_first')(x)
    # x = Dropout(rate=0.3)(x)
    # print(x)

    # x = default_conv2d(x, 256, (2,2))
    # x = MaxPool2D(data_format='channels_first')(x)
    # x = Dropout(rate=0.3)(x)
    # print(x)

    x = Flatten()(x)
    # x = Dense(1000, activation='relu')(x)
    # x = Dense(500, activation='relu')(x)
    x = Dense(1, activation='sigmoid')(x)

    return x


def conv_branch(input, start, end, res=4):
    # b = utils.Crop(1, start, end)(input)
    b = input

    b_nodes = K.int_shape(b)[1]

    b = Dense(res*res, activation='relu')(b)
    b = Reshape((b_nodes, res, res))(b)

    b = default_conv2d(b, 10, (3,3))
    return b

def conv_stream(input, cardinality, overlap=0):
    input_nodes = K.int_shape(input)[1]
    # stream_input= Input(shape=(input_nodes,), name='stream_conv_input')
    x = input
    branch_size = int(input_nodes / cardinality)

    branches = []
    for i in range(0, cardinality):
        start = i * branch_size
        end = (i+1) * branch_size

        if start > overlap-1:
            start -= overlap
        if end < input_nodes - (overlap+1):
            end += overlap

        # print(start, end)
        branches.append(conv_branch(x, start, end))


    x = Concatenate(axis=1)(branches)
    # stream_model = Model(name=get_unique_id('stream_conv_model'), inputs=[stream_input], outputs=[x])
    # x = stream_model(input)

    return x

def model_conv_parallel(input):
    x = input
    input_nodes = K.int_shape(input)[1]

    x = conv_stream(x, 40)
    x = Flatten()(x)


    # x = default_dense(x, 1000, activation='relu')
    # x = default_dense(x, 500, activation='relu')
    # x = Dropout(rate=0.5)(x)
    x = Dense(1, activation='sigmoid')(x)

    return x


def model_3d(input):
    dim = 8

    x = input
    input_nodes = K.int_shape(input)[1]


    x = default_dense(x, dim, activation='relu')
    x = Reshape((input_nodes, dim, 1))(x)
    x = default_dense(x, dim, activation='relu')
    x = Reshape((input_nodes, dim, dim, 1))(x)
    x = default_dense(x, dim, activation='relu')
    print(x)

    x = Permute((2,3,4,1))(x)
    print(x)

    x = Conv3D(16, (3,3,3), padding='same', activation=None)(x)
    x = MaxPool3D()(x)
    x = Dropout(rate=0.3)(x)

    x = Flatten()(x)

    x = Dense(1, activation='sigmoid')(x)

    return x