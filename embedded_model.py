from keras import Model, Input
from keras.layers import BatchNormalization, Flatten, Dropout, Dense, Embedding, Concatenate, ReLU, Reshape, \
    Multiply, Lambda, Activation, Softmax

from keras import backend as K

import utils
from nn_utils import get_unique_id

momentum = 0.1
# momentum = 0.9
dropout_rate=0.08
common_dropout =  Dropout(rate=dropout_rate)




def build_cat(inputs, number_of_classes, embedding_dim, identifier):
    input_nodes = K.int_shape(inputs)[1]
    inputs_cat = Input(shape=[200], name=get_unique_id('cat_embed_input', identifier))
    embedds = []
    for i in range(input_nodes):
        cropped = utils.Crop(1, i, i+1)(inputs_cat)
        embed = Embedding(number_of_classes, embedding_dim, name=get_unique_id('embed', identifier))(cropped)
        embedds.append(embed)
    x_cat_emb = Lambda(lambda x: K.stack(x, axis=1), name=get_unique_id('stack', identifier))(embedds)

    cat_embed_model = Model(inputs=[inputs_cat], outputs=[x_cat_emb], name=identifier+'_embed_model')
    x_cat_emb = cat_embed_model(inputs)
    x_cat_emb = Reshape((input_nodes, embedding_dim))(x_cat_emb)
    x_cat_emb = BatchNormalization(momentum=momentum)(x_cat_emb)
    x_cat_emb = common_dropout(x_cat_emb)


    return x_cat_emb

def build_intercept_embedd(intercept, number_of_classes, embedding_dim, input_nodes, identifier):
    x = Embedding(number_of_classes, embedding_dim)(intercept)
    x = Lambda(lambda x: K.stack(x, axis=1), name=get_unique_id('stack', identifier))([x]*input_nodes)
    x_bias_model = Model(inputs=[intercept], outputs=[x], name=identifier + '_bias_model')

    x = x_bias_model(intercept)
    x = BatchNormalization(momentum=momentum)(x)
    # x = Lambda(lambda x: K.reshape(x, (-1, input_nodes, n_emb_feat)), name=get_unique_id('k.reshape'))(x)

    return x

def build_cont(inputs_cont, x_bias, dense_sizes, identifier):
    input_nodes = K.int_shape(inputs_cont)[1]

    x = Lambda(lambda x: K.reshape(x, (-1, 1)), name=get_unique_id('k.reshape', identifier))(inputs_cont)
    x = Concatenate(axis=1)([x, x_bias])

    # x = Lambda(lambda x: K.expand_dims(x, -1), name=get_unique_id('k.expand_dims', identifier))(x)
    x = Dense(dense_sizes[0], activation='relu')(x)
    # x = Flatten()(x)

    x = Dense(dense_sizes[1], activation=None)(x)
    x = Lambda(lambda x: K.reshape(x, (-1, input_nodes, dense_sizes[1])), name=get_unique_id('k.reshape', identifier))(x)
    x = BatchNormalization(momentum=momentum)(x)
    x = common_dropout(x)

    return x


def build_embedded_model(input_dim_has_one, vocab_size_has_one, input_dim_val_counts, vocab_size_val_counts, input_dim_div, input_dim_mul, input_dim_orig, input_dim_not_u):
    # inputs = Input(shape=[input_dim_has_one + input_dim_val_counts + input_dim_div + input_dim_mul + input_dim_orig + input_dim_not_u], name='all_inputs')

    # has_one_input = utils.Crop(1, 0, 200)(inputs)
    # val_counts_input = utils.Crop(1, 200, 400)(inputs)
    # div_input = utils.Crop(1, 400, 600)(inputs)
    # mul_input = utils.Crop(1, 600, 800)(inputs)
    # orig_input = utils.Crop(1, 800, 1000)(inputs)
    # not_u_input = utils.Crop(1, 1000, 1200)(inputs)

    has_one_input = Input(shape=[input_dim_has_one], name='inputs_has_one')
    val_counts_input = Input(shape=[input_dim_val_counts], name='inputs_val_counts')
    div_input = Input(shape=[input_dim_div], name='inputs_div')
    mul_input = Input(shape=[input_dim_mul], name='inputs_mul')
    orig_input = Input(shape=[input_dim_orig], name='inputs_orig')
    not_u_input = Input(shape=[input_dim_not_u], name='inputs_not_u')


    intercept = Input(shape=[1], name='intercept')

    b_size = K.shape(has_one_input)[0]


    has_one_emb_sizes = (vocab_size_has_one, 12)
    val_counts_emb_sizes = (vocab_size_val_counts, 100)
    div_emb_sizes = (201, 2)

    val_counts_dense_size = (50, 10)
    div_dense_size = (50, 10)
    mul_dense_size = (50, 10)
    orig_dense_size = (50, 10)
    not_u_dense_size = (50, 10)

    x_bias = build_intercept_embedd(intercept, 201, 2, 200, 'x_bias')
    x_bias = Lambda(lambda x: K.reshape(x, (-1, 2)), name=get_unique_id('k.reshape', 'x_bias'))(x_bias)

    w_emb = build_intercept_embedd(intercept, 201, 2, 200, 'w_emb')


    has_one_emb = build_cat(has_one_input, has_one_emb_sizes[0], has_one_emb_sizes[1], 'has_one')
    # val_counts_emb = build_cat(val_counts_input, val_counts_emb_sizes[0], val_counts_emb_sizes[1], 'value_counts')
    val_counts_emb = build_cont(val_counts_input, x_bias, val_counts_dense_size, 'value_counts')
    div_emb = build_cont(div_input, x_bias, div_dense_size, 'div')
    mul_emb = build_cont(mul_input, x_bias, mul_dense_size, 'mul')
    orig_emb = build_cont(orig_input, x_bias, orig_dense_size, 'orig')
    not_u_emb = build_cont(not_u_input, x_bias, not_u_dense_size, 'not_u')




    #############################################
    ################# WEIGHTS ###################
    #############################################


    has_one_flat = Lambda(lambda x: K.reshape(x, (-1, has_one_emb_sizes[1])), name=get_unique_id('k.reshape', 'has_one'))(has_one_emb)
    val_counts_flat = Lambda(lambda x: K.reshape(x, (-1, val_counts_dense_size[1])), name=get_unique_id('k.reshape', 'val_counts'))(val_counts_emb)
    div_flat = Lambda(lambda x: K.reshape(x, (-1, div_dense_size[1])), name=get_unique_id('k.reshape', 'div'))(div_emb)
    mul_flat = Lambda(lambda x: K.reshape(x, (-1, mul_dense_size[1])), name=get_unique_id('k.reshape', 'mul'))(mul_emb)
    orig_flat = Lambda(lambda x: K.reshape(x, (-1, orig_dense_size[1])), name=get_unique_id('k.reshape', 'orig'))(orig_emb)
    not_u_flat = Lambda(lambda x: K.reshape(x, (-1, not_u_dense_size[1])), name=get_unique_id('k.reshape', 'not_u'))(not_u_emb)
    w_emb_flat = Lambda(lambda x: K.reshape(x, (-1, 2)), name=get_unique_id('k.reshape', 'w_emb'))(w_emb)


    # x_w = Concatenate(axis=1)([has_one_flat, w_emb_flat, orig_flat, not_u_flat])
    x_w = Concatenate(axis=1)([has_one_flat, w_emb_flat, val_counts_flat, div_flat, mul_flat, orig_flat, not_u_flat])
    # x_w = Concatenate(axis=1)([has_one_flat, div_flat, mul_flat, orig_flat, not_u_flat])
    # x_w = Concatenate(axis=1)([has_one_flat, val_counts_flat, orig_flat, not_u_flat])
    x_w = BatchNormalization(momentum=momentum)(x_w)
    x_w = Dense(20, activation='relu')(x_w)
    x_w = Dense(1, activation=None)(x_w)
    x_w = Lambda(lambda x: K.reshape(x, (b_size, 200)), name=get_unique_id('k.reshape'))(x_w)
    x_w = Activation('softmax')(x_w)
    x_w = Lambda(lambda x : K.expand_dims(x, -1), name=get_unique_id('expand_dims'))(x_w)

    has_one_emb = Multiply()([x_w, has_one_emb])
    val_counts_emb = Multiply()([x_w, val_counts_emb])
    div_emb = Multiply()([x_w, div_emb])
    mul_emb = Multiply()([x_w, mul_emb])
    orig_emb = Multiply()([x_w, orig_emb])
    not_u_emb = Multiply()([x_w, not_u_emb])

    has_one_emb = Lambda(lambda x: K.sum((x), axis=1), name=get_unique_id('sum', 'has_one'))(has_one_emb)
    val_counts_emb = Lambda(lambda x: K.sum(x, axis=1), name=get_unique_id('sum', 'val_counts'))(val_counts_emb)
    div_emb = Lambda(lambda x: K.sum(x, axis=1), name=get_unique_id('sum', 'div'))(div_emb)
    mul_emb = Lambda(lambda x: K.sum(x, axis=1), name=get_unique_id('sum', 'mul'))(mul_emb)
    orig_emb = Lambda(lambda x: K.sum(x, axis=1), name=get_unique_id('sum', 'orig'))(orig_emb)
    not_u_emb = Lambda(lambda x: K.sum(x, axis=1), name=get_unique_id('sum', 'not_u'))(not_u_emb)

    x = Concatenate(axis=1)([has_one_emb, val_counts_emb, div_emb, mul_emb, orig_emb, not_u_emb])
    # x = Concatenate(axis=1)([has_one_emb, div_emb, mul_emb, orig_emb, not_u_emb])
    # x = Concatenate(axis=1)([has_one_emb, val_counts_emb, orig_emb, not_u_emb])
    # x = Concatenate(axis=1)([has_one_emb, orig_emb, not_u_emb])


    ##########################################
    ################# TAIL ###################
    ##########################################

    x = BatchNormalization(momentum=momentum)(x)
    x = Dense(64, activation='relu')(x)
    x = BatchNormalization(momentum=momentum)(x)
    x = Dropout(rate=0.2)(x)
    x = Dense(1, activation='sigmoid')(x)

    # model = Model(
    #     inputs=[inputs,
    #             intercept],
    #     outputs=[x],
    #     name='model')

    model = Model(
        inputs=[has_one_input,
                val_counts_input,
                div_input,
                mul_input,
                orig_input,
                not_u_input,
                intercept],
        outputs=[x],
        name='model')


    return model