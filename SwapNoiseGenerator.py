import sys

import numpy as np
import keras
from sklearn.metrics import mean_squared_error

class SwapNoiseGenerator(keras.utils.Sequence):
    'Generates data for Keras'
    def __init__(self, list_IDs, samples, swap_ratio=0.15, batch_size=32, dim=(32),shuffle=True):
        'Initialization'
        self.dim = dim
        self.batch_size = batch_size
        self.samples = samples
        self.swap_ratio = swap_ratio
        self.list_IDs = list_IDs
        self.total_samples = list_IDs.shape[0]
        self.shuffle = shuffle
        self.swap_counter = self.swap_ratio
        self.on_epoch_end()

    def __len__(self):
        'Denotes the number of batches per epoch'
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        'Generate one batch of data'
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        # Generate data
        X, y = self.__data_generation(list_IDs_temp)

        return X, y

    def on_epoch_end(self):
        'Updates indexes after each epoch'
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

        print('achieved swap ratio: %.3f' % self.swap_counter)

    def __data_generation(self, list_IDs_temp):
        'Generates data containing batch_size samples' # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.empty((self.batch_size, self.dim))
        # y = np.empty((self.batch_size), dtype=int)
        X_swap = np.empty((self.batch_size, self.dim))
        swapped = 0

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            X[i,] = self.samples[ID]

            if np.random.random_sample() < self.swap_ratio:
                new_sample = np.random.randint(0, self.total_samples)
                X_swap[i,] = self.samples[new_sample]
                # print('swap! %i - using %i instead of %i' % (i, new_sample, ID))
                swapped += 1
            else:
                # print('no swap %i' % i)
                X_swap[i,] = X[i]


        self.swap_counter = swapped / len(list_IDs_temp)

        return X_swap, X