from keras import callbacks
from sklearn.metrics import roc_auc_score
import keras.backend as K
from Logger import Logger
import numpy as np

class AucLogger(Logger):
    def calculate_metric(self, *args, **kwargs):
        vd = self.validation_data
        # print(self.validation_data)
        # print(np.array(self.validation_data).shape)
        # print(self.validation_data[0])
        # print(self.validation_data[1])
        # print(self.validation_data[2])
        # print(self.validation_data[3])
        # print(self.validation_data[4])
        # print(self.validation_data[5])
        # print(self.validation_data[6])
        # cv_pred = self.model.predict(self.validation_data[0], batch_size=1024)
        inputs = []
        for i in range(len(vd)-3):
            inputs.append(vd[i])

        cv_pred = self.model.predict(inputs, batch_size=1024)
        # cv_pred = self.model.predict([self.validation_data[0],
        #                               self.validation_data[1],
        #                               self.validation_data[2]], batch_size=1024)
        # cv_pred = self.model.predict([self.validation_data[0],
        #                               self.validation_data[1],
        #                               self.validation_data[2],
        #                               self.validation_data[3],
        #                               self.validation_data[4],
        #                               self.validation_data[5],
        #                               self.validation_data[6]], batch_size=1024)
        # cv_true = self.validation_data[1]
        # cv_true = self.validation_data[7]
        # cv_true = self.validation_data[3]
        cv_true = self.validation_data[len(vd)-3]
        auc_val = roc_auc_score(cv_true, cv_pred)
        return auc_val